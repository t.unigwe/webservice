﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using Timer = System.Timers.Timer;

namespace WindowsService1
{
    public partial class Service1 : ServiceBase
    {
        Timer timer = new Timer(); // name space(using System.Timers;)  

        /// initiate the variables.
        /// </summary>
        private int _port;
        private Thread _serverThread;
        private string _rootDir;
        private HttpListener _listener;

        public Service1()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            //Create WebServerFiles directory
            string root = @"C:\WebServerFiles";
            // If directory does not exist, create it. 
            if (!Directory.Exists(root))
            {
                Directory.CreateDirectory(root);
            }

            WriteToFile("Service is started at " + DateTime.Now);
            timer.Elapsed += new ElapsedEventHandler(OnElapsedTime);
            timer.Elapsed += new ElapsedEventHandler(OnProcess);
            timer.Interval = 5000; //number in milisecinds  
            timer.Enabled = true;
            Server();
        }

        /// <summary>
        /// Method that starts the server, sets an port and creates an new thread.
        /// </summary>
        /// <param name="path"></param>
        /// <param name="port"></param>
        public void Server(string path = null, int port = 5050)
        {
            if (path == null)
            {
                path = "C:\\WebServerFiles";
            }
            this._rootDir = path;
            this._port = port;
            _serverThread = new Thread(this.Listen);
            _serverThread.Start();
        }

        /// <summary>
        /// Method that activates the process method when the user is going to the url in the web browser.
        /// </summary>
        private void Listen()
        {
            timer.Elapsed += new ElapsedEventHandler(OnListen);

            _listener = new HttpListener();
            _listener.Prefixes.Add("http://*:" + _port.ToString() + "/");
            _listener.Start();
            while (true)
            {
                try
                {
                    HttpListenerContext context = _listener.GetContext();
                    Process(context);
                }
                catch (Exception ex)
                {
                }
            }
        }

        /// <summary>
        /// Method that handles diffrent actions from the user.
        /// </summary>
        /// <param name="context"></param>
        private void Process(HttpListenerContext context)
        {
            timer.Elapsed += new ElapsedEventHandler(OnProcess);

            string filename = context.Request.Url.AbsolutePath;
            Console.WriteLine(filename);
            filename = filename.Substring(1);

            if (string.IsNullOrEmpty(filename))
            {
                filename = "index.html";
            }

            filename = Path.Combine(_rootDir, filename);

            if (File.Exists(filename))
            {
                try
                {
                    Stream input = new FileStream(filename, FileMode.Open);
                    context.Response.ContentType = "text/html";
                    context.Response.ContentLength64 = input.Length;
                    context.Response.AddHeader("Date", DateTime.Now.ToString("r"));
                    context.Response.AddHeader("Last-Modified",
                        System.IO.File.GetLastWriteTime(filename).ToString("r"));

                    byte[] buffer = new byte[1024 * 16];
                    int nbytes;
                    while ((nbytes = input.Read(buffer, 0, buffer.Length)) > 0)
                        context.Response.OutputStream.Write(buffer, 0, nbytes);
                    input.Close();

                    context.Response.StatusCode = (int)HttpStatusCode.OK;
                    context.Response.OutputStream.Flush();
                }
                catch (Exception ex)
                {
                    context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                }
            }
            else
            {
                context.Response.StatusCode = (int)HttpStatusCode.NotFound;
            }

            context.Response.OutputStream.Close();
        }

        private void OnListen(object source, ElapsedEventArgs e)
        {
            WriteToFile("Listen function");
        }

        private void OnProcess(object source, ElapsedEventArgs e)
        {
            WriteToFile("Process function");
        }

        protected override void OnStop()
        {
            _serverThread.Abort();
            _listener.Stop();
            WriteToFile("Service is stopped at " + DateTime.Now);
        }

        private void OnElapsedTime(object source, ElapsedEventArgs e)
        {
            WriteToFile("C:\\WebServerFiles");
        }

        public void WriteToFile(string Message)
        {
            string path = AppDomain.CurrentDomain.BaseDirectory + "\\Logs";
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            string filepath = AppDomain.CurrentDomain.BaseDirectory + "\\Logs\\ServiceLog_" + DateTime.Now.Date.ToShortDateString().Replace('/', '_') + ".txt";
            if (!File.Exists(filepath))
            {
                // Create a file to write to.   
                using (StreamWriter sw = File.CreateText(filepath))
                {
                    sw.WriteLine(Message);
                }
            }
            else
            {
                using (StreamWriter sw = File.AppendText(filepath))
                {
                    sw.WriteLine(Message);
                }
            }
        }
    }
}